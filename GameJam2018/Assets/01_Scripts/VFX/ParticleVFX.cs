﻿namespace SpecterOps.Utilities
{
    using UnityEngine;
    using System.Collections;

    /// <summary>
    /// Particle vfx life manager
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleVFX : MonoBehaviour
    {
        // Core components
        public ParticleSystem ParticleSystem;
        public bool Destroy = false;

        /// <summary>
        /// Force particle emission on start
        /// </summary>
        private void OnEnable()
        {
            this.ParticleSystem.Play();
            StartCoroutine("AutoDisabler");
        }

        /// <summary>
        /// If deactivated prematurely, stop particle system and coroutine
        /// </summary>
        private void OnDisable()
        {
            this.ParticleSystem.Stop();
            StopCoroutine("AutoDisabler");
        }

        /// <summary>
        /// Disable the particle system when it stops playing
        /// </summary>
        /// <returns></returns>
        IEnumerator AutoDisabler()
        {
            while (this.ParticleSystem.isPlaying)
                yield return null;
            if (!Destroy)
                this.gameObject.SetActive(false);
            else
                Destroy(this.gameObject);
        }

    }
}
