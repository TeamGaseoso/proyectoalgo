﻿using UnityEngine;

namespace Audio
{
	public class PlayAudioClip : MonoBehaviour 
	{
		public AudioSource AudioSource;
		public AudioClip AudioClip;

		public void Awake()
		{
			if (AudioSource == null)
			{
				AudioSource = gameObject.AddComponent<AudioSource>();
			}
		}

		public void PlayClip()
		{
			AudioSource.clip = AudioClip;
			AudioSource.Play();
		}
	}
}
