﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
	[Serializable]
	public class GameSFX
	{
		public string Identifier;
		public AudioSource Source;
		public AudioClip Clip;
		[Range(0, 1)]
		public float Volume = 1f;
		[Range(.25f, 3)]
		public float Pitch = 1f;
		public bool Loop = false;
		[Range(0f, 1f)]
		public float SpacialBlend = 1f;

		public void SetSourceProperties(AudioClip clip, float volume, float picth, bool loop, float spacialBlend)
		{
			Source.clip = clip;
			Source.volume = volume;
			Source.pitch = picth;
			Source.loop = loop;
			Source.spatialBlend = spacialBlend;
		}

		public void Play()
		{
			SetSourceProperties(Clip, Volume, Pitch, Loop, SpacialBlend);
			Source.Play();
		}

		public void Stop()
		{
			Source.Stop();
		}
	}

	public class AudioSourceController : MonoBehaviour
	{
		[SerializeField] private List<GameSFX> _sfxs = new List<GameSFX>();
		private Dictionary<string, GameSFX> _sfxsDictionary = new Dictionary<string, GameSFX>();

		private GameObject _instancedAudioSources = null;

		void Awake()
		{
			for (int i = 0; i < _sfxs.Count; i++)
			{
				if (_sfxs[i].Source == null)
				{
					if (!_instancedAudioSources)
					{
						_instancedAudioSources = new GameObject("AudioSources");
						_instancedAudioSources.transform.position = transform.position;
						_instancedAudioSources.transform.SetParent(transform);
					}

					_sfxs[i].Source = _instancedAudioSources.AddComponent<AudioSource>();
					_sfxs[i].Source.playOnAwake = false;
				}

				_sfxsDictionary.Add(_sfxs[i].Identifier, _sfxs[i]);
			}
		}

		public void Play(string identifier)
		{
			GameSFX sfx;

			if (_sfxsDictionary.TryGetValue(identifier, out sfx))
			{
				sfx.Play();
			}
		}

		public void Stop(string identifier)
		{
			GameSFX sfx;

			if (_sfxsDictionary.TryGetValue(identifier, out sfx))
			{
				sfx.Stop();
			}
		}
	}
}