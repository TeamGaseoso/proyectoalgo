﻿using General;
using UnityEngine;

namespace Mechanics
{
	public class FlagCapturer : MonoBehaviour
	{
		[SerializeField] private int _playerScoreId;
		[SerializeField] private float _rotatingDistance = 0.5f;
		[SerializeField] private float _rotatingSpeed = 500.0f;
        [SerializeField] private GameObject obtainedPS;
		private GameObject _flag = null;

		public int PlayerScoreId { get { return _playerScoreId;  } }

        public void CaptureFlag(GameObject flagObject)
		{
            if (_flag != null)
                return;

			_flag = flagObject;

            if (obtainedPS != null)
                obtainedPS.SetActive(true);

			EventManager.TriggerEvent("Flag Capture");

            AvatarComponent.Instance.SetAvatarAsCardHolder(this.gameObject);
        }

        public void RemoveFlag()
		{
			_flag = null;
		}

		private void Update()
		{
			if (_flag != null)
			{
				_flag.transform.position = transform.position + (_flag.transform.position - transform.position).normalized * _rotatingDistance;
				_flag.transform.RotateAround(transform.position, transform.up, Time.deltaTime * _rotatingSpeed);
			}
		}
	}
}
