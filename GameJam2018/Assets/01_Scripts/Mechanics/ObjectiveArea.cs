﻿using General;
using Managers;
using UI;
using UnityEngine;

namespace Mechanics
{
	[RequireComponent(typeof(Collider))]
	public class ObjectiveArea : MonoBehaviour
	{
		private PlayerFlag _playerFlag;
		private Collider _collider;
		private MeshRenderer _meshRenderer;

        [SerializeField]
        private GameObject particles;

		private void Awake()
		{
			_collider = GetComponent<Collider>();
			_meshRenderer = GetComponent<MeshRenderer>();
			_collider.enabled = false;
		}

        public GameObject successParticle;

		private void OnTriggerEnter(Collider collider)
		{
			// Check if a player flag is entering the objective area
			PlayerFlag capturer = collider.GetComponent<PlayerFlag>();

			if (capturer != null)
			{
                successParticle.gameObject.SetActive(true);

                AvatarComponent.Instance.RemoveAllAvatars();

                ShakeCameraComponent.Instance.WinShake();

                capturer.Owner.RemoveFlag();
				FlagRepositionArea.Instance.RepositionFlag();
				GameManager.Instance.IncreaseScore(capturer.Owner.PlayerScoreId);

			}
		}

		public void DeactivateArea()
		{
            particles.SetActive(_meshRenderer.enabled = _collider.enabled = false);

		}

		public void ActivateArea()
		{
            particles.SetActive(_meshRenderer.enabled = _collider.enabled = true);
        }
    }
}
