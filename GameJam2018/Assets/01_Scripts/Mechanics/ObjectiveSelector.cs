﻿using System;
using System.Collections.Generic;
using General;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Mechanics
{
	public class ObjectiveSelector : MonoBehaviour
	{
		[SerializeField] private List<ObjectiveArea> _possibleAreas;

		private void Start()
		{
			SelectRandomObjective();
		}

		private void OnEnable()
		{
			EventManager.StartListening("Flag Capture", SelectRandomObjective);
		}

		private void OnDisable()
		{
			EventManager.StopListening("Flag Capture", SelectRandomObjective);
		}

		private void SelectRandomObjective()
		{
			int index = Random.Range(0, _possibleAreas.Count - 1);

			for (int i = 0; i < _possibleAreas.Count; i++)
			{
				if (i != index)
				{
					_possibleAreas[i].DeactivateArea();
				}
				else
				{
					_possibleAreas[i].ActivateArea();
				}
			}
		}
	}
}
