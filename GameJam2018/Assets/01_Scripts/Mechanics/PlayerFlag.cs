﻿using Audio;
using UnityEngine;

namespace Mechanics
{
	[RequireComponent(typeof(Collider))]
	public class PlayerFlag : MonoBehaviour
	{
		[SerializeField] private AudioSourceController _audioPlayer;
		private FlagCapturer _currentOwner;

		public FlagCapturer Owner
		{
			get { return _currentOwner; }
		}

		private void Awake()
		{
			if (_audioPlayer == null)
			{
				_audioPlayer = GetComponent<AudioSourceController>();
			}
		}

		private void OnTriggerEnter(Collider collider)
		{
			// Check if the collider object is a flag capturer
			FlagCapturer capturer = collider.GetComponent<FlagCapturer>();

			if (capturer != null)
			{
				if (_currentOwner != null && _currentOwner != capturer)
				{
					_currentOwner.RemoveFlag();
				}

				_currentOwner = capturer;
				_currentOwner.CaptureFlag(gameObject);
				_audioPlayer.Play("Flag Capture");
			}
		}
	}
}
