﻿using System.Collections.Generic;
using General;
using UnityEngine;

namespace Mechanics
{
	public class FlagRepositionArea : Singleton<FlagRepositionArea>
	{
		[SerializeField] private GameObject _flagObject;
		[SerializeField] private List<BoxCollider> _repositionAreas;
		private List<Vector3> _maxPosition;
		private List<Vector3> _minPosition;

		private void Awake()
		{
			_maxPosition = new List<Vector3>();
			_minPosition = new List<Vector3>();

			for (int i = 0; i < _repositionAreas.Count; i++)
			{
				_maxPosition.Add(_repositionAreas[i].bounds.max);
				_minPosition.Add(_repositionAreas[i].bounds.min);
			}
		}

		public void RepositionFlag()
		{
			int index = Random.Range(0, _repositionAreas.Count - 1);
			Vector3 max = _maxPosition[index];
			Vector3 min = _minPosition[index];
			Vector3 nextPos = new Vector3(Random.Range(max.x, min.x), Random.Range(max.y, min.y), Random.Range(max.z, min.z));

			_flagObject.transform.position = nextPos;
		}
	}
}
