﻿using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class WinningUI : MonoBehaviour
	{
        public Sprite p1Won;
        public Sprite p2Won;

        public Image winImage;

		private void Start()
		{
            if(GameManager.Instance.WinningPlayer == 0)
                winImage.sprite = p1Won;
            else
                winImage.sprite = p2Won;
        }

        public void RestartGame()
		{
			GameManager.Instance.RestartGame();
			;
		}

		public void QuitGame()
		{
			GameManager.Instance.QuitGame();
		}
	}
}
