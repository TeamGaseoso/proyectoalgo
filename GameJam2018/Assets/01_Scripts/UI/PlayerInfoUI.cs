﻿using System;
using System.Globalization;
using General;
using Movement;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class PlayerInfoUI : MonoBehaviour
	{
		[SerializeField] private DashMovement _dashMovement;
		[SerializeField] private Image _coldownImage;

		private float _dashPressTime = 0.0f;

		private void OnEnable()
		{
			EventManager.StartListening("Dash Trigger" + _dashMovement.GetInstanceID(), UpdateTimer);
		}

		private void OnDisable()
		{
			EventManager.StopListening("Dash Trigger" + _dashMovement.GetInstanceID(), UpdateTimer);
		}

		private void UpdateTimer()
		{
			_dashPressTime = Time.time;
		}

		private void Update()
		{
			if (_dashMovement.IsOnCooldown)
            {
                float currentColdown = (_dashMovement.DashCooldown - (Time.time - _dashPressTime));
                _coldownImage.fillAmount = Mathf.Lerp(0.44f, 1f, 1.0f - currentColdown / _dashMovement.DashCooldown);
			}
		}
	}
}
