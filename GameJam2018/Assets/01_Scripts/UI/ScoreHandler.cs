﻿using System.Collections.Generic;
using General;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class ScoreHandler : Singleton<ScoreHandler>
	{
		[SerializeField] private List<Text> _playerScores;
		[SerializeField] private Text _goalScore;
		private List<int> _scores;

		private void Awake()
		{
			_scores = new List<int>();

			for (int i = 0; i < _playerScores.Count; i++)
			{
				_playerScores[i].text = "0";
				_scores.Add(0);
			}

			_goalScore.text = GameManager.Instance.GoalScore.ToString();
		}

		public void IncreaseScore(int playerId)
		{
			_scores[playerId]++;
			_playerScores[playerId].text = _scores[playerId].ToString();
		}
	}
}
