﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace General
{
	public class SceneLoader : MonoBehaviour
	{
		// Scene loading
		public string MainSceneString = "MainMenu";
		public string GameSceneString = "GameScene";
		public string CreditsSceneString = "CreditsScene";
		public string WinningScene = "WinningScene";

        public GameObject credits;

		// Load main scene
		public void LoadMainScene()
		{
            credits.gameObject.SetActive(false);
        }

        // Load game scene
        public void LoadGameScene()
		{
			SceneManager.LoadScene(this.GameSceneString);
		}

		// Load game scene
		public void LoadCreditsScene()
		{
            credits.gameObject.SetActive(true);
        }

		// Load game scene
		public void LoadWinningScene()
		{
			SceneManager.LoadScene(this.WinningScene);
		}
	}
}