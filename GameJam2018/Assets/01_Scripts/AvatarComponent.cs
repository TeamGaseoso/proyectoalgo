﻿using System.Collections;
using Audio;
using General;
using UnityEngine;


public class AvatarComponent : Singleton<AvatarComponent> {

    public Player PlayerA;
    public Player PlayerB;

    [System.Serializable]
    public struct Player
    {
        public GameObject player;
        public GameObject good;
        public GameObject bad;
        public GameObject neutral;
    }

    public enum Type
    {
        Neutral,
        Bad,
        Good
    }

    public void SetAvatarAsCardHolder(GameObject holder)
    {
        if(PlayerA.player == holder)
        {
            SetAvatarType(PlayerA, Type.Good);
            SetAvatarType(PlayerB, Type.Bad);
        }
        else
        {
            SetAvatarType(PlayerA, Type.Bad);
            SetAvatarType(PlayerB, Type.Good);
        }
    }

    public void RemoveAllAvatars()
    {
        SetAvatarType(PlayerA,Type.Neutral);
        SetAvatarType(PlayerB, Type.Neutral);
    }

    public void SetAvatarType(Player player, Type type)
    {
        player.bad.SetActive(false);
        player.neutral.SetActive(false);
        player.good.SetActive(false);

        switch (type)
        {
            case Type.Neutral:
                player.neutral.GetComponent<TrailRenderer>().Clear();
                player.neutral.SetActive(true);
                break;
            case Type.Bad:
                player.bad.GetComponent<TrailRenderer>().Clear();
                player.bad.SetActive(true);
                break;
            case Type.Good:
                player.good.GetComponent<TrailRenderer>().Clear();
                player.good.SetActive(true);
                break;

        }
    }

}
