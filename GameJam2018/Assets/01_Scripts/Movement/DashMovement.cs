﻿using System;
using System.Collections;
using Audio;
using General;
using UnityEngine;
using UnityEngine.Networking;

namespace Movement
{
	[RequireComponent(typeof(PlayerMovement))]
	public class DashMovement : MonoBehaviour
	{
		[SerializeField] private KeyCode _dashKeyCode;
		[SerializeField] private KeyCode _dashGamepadCode;
		[SerializeField] private float _maxChargeTime = 1.0f;
		[SerializeField] private float _dashForce = 50.0f;
		[SerializeField] private float _cooldown = 3.0f;
		[SerializeField] private AnimationCurve _chargeTimeScale;
		[SerializeField] private float _inputBreakTolerance;
        [SerializeField] private GameObject _dashParticle;
		[SerializeField] private AudioSourceController _audioPlayer;
        [SerializeField] private GameObject _dashChargeParticle;

        private PlayerMovement _playerMovement;
		private bool _isOnCooldown;
		private float _keyPressTime;
		private float _keyDownTime;
		private bool _dashReady = false;

		public float DashCooldown { get { return _cooldown; } }
		public bool IsOnCooldown { get { return _isOnCooldown; } }
		public float CurrentCharge { get { return (Time.time - _keyDownTime) / _maxChargeTime; } }
		public bool DashReady { get { return _dashReady;  } }

		private void Awake()
		{
			if (_audioPlayer == null)
			{
				_audioPlayer = GetComponent<AudioSourceController>();
			}

			_playerMovement = GetComponent<PlayerMovement>();
		}

		private void OnEnable()
		{
			EventManager.StartListening("Dash Trigger" + this.GetInstanceID(), Dash);
		}

		private void OnDisable()
		{
			EventManager.StopListening("Dash Trigger" + this.GetInstanceID(), Dash);
		}

		private void Update()
		{
            if (_isOnCooldown) return;


            if (Input.GetKey(_dashKeyCode))
            {
                _dashChargeParticle.SetActive(true);
            }
            else
            {
                _dashChargeParticle.SetActive(false);

            }

            if ((Input.GetKeyDown(_dashKeyCode) || Input.GetKeyDown(_dashGamepadCode)) && !_dashReady)
			{
				_audioPlayer.Play("Dash Charge");
				_keyDownTime = Time.time;
				_keyPressTime = 0.0f;
				_dashReady = true;
            }

            if (Time.time >= (_keyDownTime + _maxChargeTime) && _dashReady)
			{
				_dashReady = false;
				_keyPressTime = _maxChargeTime;
				Cooldown().Start();
                EventManager.TriggerEvent("Dash Trigger" + this.GetInstanceID());
			}
			else
			{
				if (Input.GetKeyUp(_dashKeyCode))
				{
					_dashReady = false;
					_keyPressTime = Time.time - _keyDownTime;
					Cooldown().Start();
                    _dashChargeParticle.SetActive(false);

                    EventManager.TriggerEvent("Dash Trigger" + this.GetInstanceID());
				}
			}
		}

		private IEnumerator Cooldown()
		{
			_isOnCooldown = true;
			yield return new WaitForSeconds(_cooldown);
			_isOnCooldown = false;
		}

		private void Dash()
		{
			Debug.Log("Pressed Dash");

			if (_playerMovement)
			{
				// Normalize press time
				float normalizedPressTime = _keyPressTime / _maxChargeTime;
				float finalDashForce = _chargeTimeScale.Evaluate(normalizedPressTime) * _dashForce;

				_playerMovement.AddForce(finalDashForce);
				_audioPlayer.Stop("Dash Charge");
				_audioPlayer.Play("Dash");

                _dashParticle.SetActive(true);
                _dashChargeParticle.SetActive(false);

                ToggleInput().Start();
			}
		}

		private IEnumerator ToggleInput()
		{
			_playerMovement.EnableInput = false;

			while (_playerMovement.Velocity.sqrMagnitude > _inputBreakTolerance)
			{
				yield return null;
			}

			_playerMovement.EnableInput = true;
		}
	}
}
