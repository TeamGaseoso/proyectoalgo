﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Movement
{
	public class PlayerMovement : MonoBehaviour
	{
		[SerializeField] private string _playerPostfix;
		[SerializeField] private float _speed = 6f;
		[SerializeField] private Rigidbody _rigidbody;
		private Vector3 _movement = Vector3.zero;

        private List<Animator> _animators;
        [SerializeField]
        private List<GameObject> _movementPS;


        public bool EnableInput { get; set; }
		public Vector3 Velocity { get { return _rigidbody.velocity; } }

		private void Awake()
		{
			EnableInput = true;

            _animators = GetComponentsInChildren<Animator>(true).ToList();

            if (_rigidbody == null) _rigidbody = GetComponent<Rigidbody>();
		}

		private void FixedUpdate()
		{
			float h = 0f;
			float v = 0f;

			// Store player input
			if (EnableInput)
			{
				h = Input.GetAxisRaw("Horizontal" + _playerPostfix);
				v = Input.GetAxisRaw("Vertical" + _playerPostfix);
			}
		
			// Move the player around
			Move(h, v);
		}

		private void Move(float h, float v)
		{
            _movement.Set(h, 0.0f, v);

            // Rotate the player and animate it
            if (_movement.magnitude > 0)
            {
                Vector3 relativePos = (transform.position + _movement) - transform.position;
                Quaternion rotation = Quaternion.LookRotation(relativePos);
                transform.rotation = rotation;

                foreach (var anim in _animators)
                {
                    anim.SetBool("Run", true);
                }
            }
            else
            {
                foreach(var anim in _animators)
                {
                    anim.SetBool("Run", false);
                }
            }

            // Update movement dependant ps
            foreach(var obj in _movementPS)
            {
                obj.SetActive(_movement.magnitude > 0);
            }

            // Normalise and scale movement according to speed
            _movement = _movement.normalized * _speed * Time.deltaTime;

            // Check if the body's current velocity will result in a collision
            RaycastHit hit;
            float distance = _movement.magnitude * Time.fixedDeltaTime;
            if (_rigidbody.SweepTest(transform.position + _movement, out hit, distance))
            {
                // If so, stop the movement
                _rigidbody.velocity = new Vector3(0, _rigidbody.velocity.y, 0);
            }
            else
            {
                // Move the player
                _rigidbody.MovePosition(transform.position + _movement);
            }
        }

		public void AddForce(float boost)
		{
			_rigidbody.AddForce(transform.forward * boost, ForceMode.VelocityChange);
		}
	}
}
