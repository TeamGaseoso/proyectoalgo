﻿using System.Collections.Generic;
using General;
using UI;
using UnityEngine;

namespace Managers
{
	public class GameManager : Singleton<GameManager>
	{
		[SerializeField] private int _goalScore;
		[SerializeField] private SceneLoader _sceneLoader;
		private Dictionary<int, int> _playerScores;

		public int WinningPlayer { get; private set; }
		public int GoalScore { get { return _goalScore; } }
		public Dictionary<int, int> PlayerScores { get { return _playerScores; } }

		public void Awake()
		{
			if (_sceneLoader == null)
			{
				_sceneLoader = GetComponent<SceneLoader>();
			}

			_playerScores = new Dictionary<int, int>();
			WinningPlayer = -1;
		}

		public void IncreaseScore(int playerId)
		{
			int score;

			if (_playerScores.TryGetValue(playerId, out score))
			{
				_playerScores[playerId]++;
			}
			else
			{
				_playerScores.Add(playerId, 1);
			}

			ScoreHandler.Instance.IncreaseScore(playerId);

			if (_playerScores[playerId] == _goalScore)
			{
				WinningPlayer = playerId;
				EventManager.TriggerEvent("Player Won");
				_sceneLoader.LoadWinningScene();
			}
		}

		public void RestartGame()
		{
			_sceneLoader.LoadGameScene();
		}

		public void QuitGame()
		{
			Application.Quit();
		}
	}
}
