﻿using System.Collections;
using Audio;
using General;
using UnityEngine;
using DG.Tweening;

public class ShakeCameraComponent : Singleton<ShakeCameraComponent> {
    public float WinShakeDuration = 1.0f;
    public float WinShakeStrenght = 2.0f;

    public void WinShake()
    {
        this.gameObject.transform.DOKill();
        this.gameObject.transform.localPosition = Vector3.zero;
        this.gameObject.transform.DOShakePosition(WinShakeDuration, WinShakeStrenght).OnComplete(() =>
         {
             this.gameObject.transform.localPosition = Vector3.zero;
         });
    }
}
